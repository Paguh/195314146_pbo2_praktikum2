package ukm22;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
public class tambahMasyarakat_dialog extends JDialog implements ActionListener{
    private JLabel label_title;
    private JButton button_ok;
    private JLabel label_nama;
    private JLabel label_nomor;
    private JLabel label_ttl;
    private JTextField textField_nama;
    private JTextField textField_nomor;
    private JTextField textField_ttl;
    public tambahMasyarakat_dialog(){
        init();
    }
    public void init(){
        this.setLayout(null);   
        label_title = new JLabel("Form Tambah Masyarakat Umum");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);
        label_nama = new JLabel("Nama \t :");
        label_nama.setBounds(122, 60, 100, 50);
        this.add(label_nama);
        textField_nama = new JTextField();
        textField_nama.setBounds(180, 75, 180, 20);
        this.add(textField_nama);
        label_nomor = new JLabel("Nomor \t :");
        label_nomor.setBounds(117, 100, 100, 50);
        this.add(label_nomor);
        textField_nomor = new JTextField();
        textField_nomor.setBounds(180, 115, 180, 20);
        this.add(textField_nomor);
        label_ttl = new JLabel("Tempat & Tanggal Lahir \t :");
        label_ttl.setBounds(20, 155, 200, 20);
        this.add(label_ttl);
        textField_ttl = new JTextField();
        textField_ttl.setBounds(180, 155, 180, 20);
        this.add(textField_ttl);
        button_ok = new JButton("OK");
        button_ok.setBounds(150, 200, 100, 30);
        this.add(button_ok);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button_ok) {
            int j = 5;
            if (mainFrame.jumlah <= j) {

                MasyarakatSekitar mhs = new MasyarakatSekitar();
                mhs.setNama(textField_nama.getText());
                mhs.setNomor (textField_nomor.getText());
                mhs.setTempatTanggalLahir(textField_ttl.getText());
                mainFrame.anggota[mainFrame.jumlah] = mhs;
                mainFrame.jumlah++;
                JOptionPane.showMessageDialog(null, textField_nama.getText() + "\nData Masyarakat tersimpan");
                this.dispose();
            }
    }
    
    }
}
