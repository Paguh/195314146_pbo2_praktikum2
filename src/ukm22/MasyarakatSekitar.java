package ukm22;
public class MasyarakatSekitar extends Penduduk{
    private String nomor;

    public MasyarakatSekitar(String nomor){
        this.nomor = nomor;
    }
    public MasyarakatSekitar() {
    }
    public MasyarakatSekitar(String dataNis, String dataNama, String dataTempatTanggalLahir){
        
    }
    public String getNomor() {
        return nomor;
    }
    public void setNomor(String nomor) {
        this.nomor = nomor;
    }
    @Override
    public double hitungIuran(){
        double value = Double.parseDouble(nomor);
        double iuran = value*100;
        return iuran;
    }    
}
